import mlrose
import time
import numpy as np

fitness_functions = [('flip flop', mlrose.FlipFlop()), ('six peaks', mlrose.SixPeaks())]
input_lengths = [10, 30, 40, 50, 60, 70, 80, 100]
knapsack = True
# fitness_functions = [('one max', mlrose.OneMax())]
# input_lengths = [100]

file_name = str(time.time())
results_file = open("results/{}.txt".format(file_name),"w")
# scores = {10: [], 30: [], 40: [], 50: [], 60: [], 70: [], 80: [], 100: []}

total_time_start = time.time()
# for i in range(5):
for fitness in fitness_functions:
    for input_len in input_lengths:
        print()
        print('fitness: {}, length: {}'.format(fitness[0], input_len))
        results_file.write('\n')
        results_file.write('fitness: {}, length: {}'.format(fitness[0], input_len))

        if knapsack:
            init_state = np.zeros(input_len)
            weights = np.arange(1,input_len+1)
            values = np.random.randint(1, input_len, size=input_len)
            problem = mlrose.DiscreteOpt(length = input_len, fitness_fn = mlrose.Knapsack(weights, values, max_weight_pct=0.35), maximize = True)
            
        else:
            problem = mlrose.DiscreteOpt(length = input_len, fitness_fn = fitness[1], maximize = True)
            init_state = np.zeros(input_len)
            
        schedule = mlrose.ExpDecay()
        # np.random.seed(2)

        timer_start = time.time()
        best_state, best_fitness, curve = mlrose.random_hill_climb(problem, max_iters=1000, max_attempts=1000, init_state=init_state, curve=True)
        timer = time.time() - timer_start
        output = '\nrandom_hill_climb\nbest state: {}\nbest fitness: {}\nevaluations: {}\ntime: {}'.format(best_state, best_fitness, len(curve), round(timer, 4))
        print(output)
        results_file.write(output)
        results_file.write('\n')

        timer_start = time.time()
        best_state, best_fitness, curve = mlrose.simulated_annealing(problem, max_iters=1000, max_attempts=1000, init_state=init_state, schedule=schedule, curve=True)
        timer = time.time() - timer_start
        output = '\nsimulated_annealing\nbest state: {}\nbest fitness: {}\nevaluations: {}\ntime: {}'.format(best_state, best_fitness, len(curve), round(timer, 4))
        print(output)
        results_file.write(output)
        results_file.write('\n')

        timer_start = time.time()
        best_state, best_fitness, curve = mlrose.genetic_alg(problem, max_iters=1000, max_attempts=1000, curve=True,  mutation_prob=0.5)
        timer = time.time() - timer_start
        output = '\ngenetic_alg\nbest state: {}\nbest fitness: {}\nevaluations: {}\ntime: {}'.format(best_state, best_fitness, len(curve), round(timer, 4))
        print(output)
        results_file.write(output)
        results_file.write('\n')
        # scores[input_len].append(best_fitness)

        timer_start = time.time()
        best_state, best_fitness, curve = mlrose.mimic(problem, max_iters=1000, max_attempts=1000, curve=True)
        timer = time.time() - timer_start
        output = '\nmimic\nbest state: {}\nbest fitness: {}\nevaluations: {}\ntime: {}'.format(best_state, best_fitness, len(curve), round(timer, 4))
        print(output)
        results_file.write(output)
        results_file.write('\n')

total_time = time.time() - total_time_start
output = 'total time: {}'.format(round(total_time, 4))
print(output)
results_file.write('\n')
results_file.write(output)
results_file.close()

# for k in scores.keys():
#     print(np.mean(scores[k]))